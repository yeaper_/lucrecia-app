import React from "react";
import CustomTable from "../components/CustomTable";

const ViewPage = () => {
  return <CustomTable />;
};

export default ViewPage;
