import React from "react";
import CustomTable from "../components/CustomTable";

const InitPage = () => {
  return (
    <div className="ui container">
      <CustomTable />
    </div>
  );
};

export default InitPage;
